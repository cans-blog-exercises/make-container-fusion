import pytest
from pytest_bdd import scenarios, given, when, then, parsers

import os
import subprocess

scenarios("../features/order-predence.feature")
scenarios("../features/radix.feature")

@pytest.fixture
def jsoncalc_args():
    args = []
    yield args

@given(parsers.parse('{file_name} file with contents\n"""{file_content}"""'))
def create_file(file_name, file_content, tmpdir):
    file = tmpdir.join(file_name)
    file.write(file_content)

@when(parsers.parse('I run program with args "{arguments}"'))
def insert_args(arguments, jsoncalc_args):
    for arg in arguments.split():
        jsoncalc_args.append(arg)

@then(parsers.parse("The output should be {expected_value}"))
def run_and_check_output(expected_value, jsoncalc_args, tmpdir):
    jsoncalc_path = os.path.realpath("jsoncalc")
    res = subprocess.check_output([jsoncalc_path] + jsoncalc_args, cwd=tmpdir).strip().decode()
    assert res == expected_value
