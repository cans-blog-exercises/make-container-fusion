Feature: JsonCalc can use numbers with any radix

    Scenario: base 10 and base 2
        Given number1.json file with contents
            """{
                "radix": 10,
                "value": 1
            }"""
        And number2.json file with contents
            """{
                "radix": 2,
                "value": 10
            }"""
        When I run program with args "number1.json + number2.json"
        Then the output should be 3

    Scenario: base 10 and base 8
        Given number1.json file with contents
            """{
                "radix": 10,
                "value": 1
            }"""
        And number15.json file with contents
            """{
                "radix": 8,
                "value": 17
            }"""
        When I run program with args "number1.json + number15.json"
        Then the output should be 16
