Feature: JsonCalc honors operator precedence

    Scenario: equal operator precedence
        Given number1.json file with contents
            """{
                "radix": 10,
                "value": 1
            }"""
        And number2.json file with contents
            """{
                "radix": 10,
                "value": 2
            }"""
        And number3.json file with contents
            """{
                "radix": 10,
                "value": 3
            }"""
        When I run program with args "number1.json - number2.json + number3.json"
        Then the output should be 2

    Scenario: different operator precedence
        Given number1.json file with contents
            """{
                "radix": 10,
                "value": 1
            }"""
        And number2.json file with contents
            """{
                "radix": 10,
                "value": 2
            }"""
        And number3.json file with contents
            """{
                "radix": 10,
                "value": 3
            }"""
        When I run program with args "number1.json + number2.json * number3.json"
        Then the output should be 7

    Scenario: different operator precedence 2
        Given number1.json file with contents
            """{
                "radix": 10,
                "value": 1
            }"""
        And number2.json file with contents
            """{
                "radix": 10,
                "value": 2
            }"""
        And number3.json file with contents
            """{
                "radix": 10,
                "value": 3
            }"""
        When I run program with args "number1.json * number2.json + number3.json"
        Then the output should be 5
