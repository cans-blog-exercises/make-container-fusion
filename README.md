# JsonCalc
A simple program to demonstrate make & container fusion.

Along with `make build` and `make_component_tests` targets, these can be run inside docker by prefixing
targets `docker_`. E.g. `make docker_component_test`.

## About JsonCalc
JsonCalc is just your ordinary calculator with a twist, the numbers are stored with json files. And the
numbers can be in any radix.

The json schema for the numbers are like the following binary number of 11.
```json
{
	"radix": 2,
	"value": 1011
}
```

### Example run

```sh
[0] [00:01] [can@homebook jsoncalc]
$ ls -1
jsoncalc
number1.json
number2.json
number3.json
[0] [00:01] [can@homebook jsoncalc]
$ jq < number1.json # 7
{
  "radix": 10,
  "value": 7
}
[0] [00:02] [can@homebook jsoncalc]
$ jq < number2.json # 133
{
  "radix": 8,
  "value": 205
}
[0] [00:03] [can@homebook jsoncalc]
$ jq < number3.json # 10
{
  "radix": 2,
  "value": 1010
}
[0] [00:04] [can@homebook jsoncalc]
$ jsoncalc number1.json '+' number2.json '*' number3.json # 7 + 133 * 10
1337
```

## Build & Test
Run `make docker_build` or `make docker_component_test`. Make sure you have docker installed on your system, that's all.

## How It works
This is realized with below Makefile directives.

```make
# Usage: docker_cmd <additional-run-options> <image-directory-under-container> <args>
define docker_cmd
	@# Strip whitespaces.
	image_name=$(shell expr $(2) : "\(\S\+\)") && \
			   docker build --tag "jsoncalc:$${image_name}" "container/$${image_name}" && \
	docker run \
		$(DOCKER_OPTS) \
		$(1) \
		"jsoncalc:$${image_name}" \
		$(3)
endef

# Array of variables to feed into docker environment.
DOCKER_ENV_VARS = \
	TERM

DOCKER_OPTS = \
	--interactive \
	--tty \
	--rm \
	--volume '$(CURDIR):$(CURDIR)' \
	--workdir '$(CURDIR)' \
	--user $(shell id --user):$(shell id --group) \
	$(foreach var, $(DOCKER_ENV_VARS), --env $(var)) \

.PHONY: docker_%
docker_%:
	$(call docker_cmd ,\
		$(DOCKER_EXTRA_OPTS) ,\
		$(subst docker_,,$@) ,\
		$(DOCKER_PRE_CMD) $(MAKE) $(subst docker_,,$@) \
	)

docker_component_test: docker_build
```

 1. `docker_cmd`
 You can think this of as a function inside Makefile with specified arguments. This function automatically
 deduces the path for Dockerfile from It's target name and builds required container image for relevant
 target. After building is complete, It runs the arguments supplied to is as if it was ran under container.

 	E.g. `make docker_build` would look for `containers/build/Dockerfile` and build it. After that, it would
 	spin up the built container and issue the command provided with `args` parameter under container.

 2. `docker_env_vars` and `docker_ops`
 These are for configurations of environment variables that should be forwarded from host to container and
 default docker options to access source/test files with same host uid/gid, respectively.

 3. `docker_%`
 This is a wildcard make target that captures every `docker_<actual_target>` calls. Essentially this uses
 the `docker_cmd` function to build the relevant container image and spin it up. What critical here is, it
 deduces real target from issued command and run the very same make command under the container. What good
 about this is this would be automatically applicable for any future targets defined inside the Makefile.

 	E.g. for `make docker_build` it would issue `make build` command under relevant container.

 4. `docker_component_test: docker_build`
 This is for to keep same target dependencies as `component_test` target already requires `build` target.
