PROJECT_NAME ?= jsoncalc
IMAGE_NAME_TAG = $(PROJECT_NAME)

all: build

build: jsoncalc
jsoncalc: src/jsoncalc.cpp
	g++ -Wall -Wextra $^ -o $@

.PHONY:
component_test: jsoncalc
	python3 -m pytest \
		--basetemp=tmp_pytest \
		--verbose \
		test/steps

# Usage: docker_cmd <additional-run-options> <image-directory-under-container> <args>
define docker_cmd
	@# Strip whitespaces.
	image_name=$(shell expr $(2) : "\(\S\+\)") && \
			   docker build --tag "$(IMAGE_NAME_TAG):$${image_name}" "container/$${image_name}" && \
	docker run \
		$(DOCKER_OPTS) \
		$(1) \
		"$(IMAGE_NAME_TAG):$${image_name}" \
		$(3)
endef

# Array of variables to feed into docker environment.
DOCKER_ENV_VARS = \
	TERM

DOCKER_OPTS = \
	--interactive \
	--tty \
	--rm \
	--volume '$(CURDIR):$(CURDIR)' \
	--workdir '$(CURDIR)' \
	--user $(shell id --user):$(shell id --group) \
	$(foreach var, $(DOCKER_ENV_VARS), --env $(var)) \

.PHONY: docker_%
docker_%:
	$(call docker_cmd ,\
		$(DOCKER_EXTRA_OPTS) ,\
		$(subst docker_,,$@) ,\
		$(DOCKER_PRE_CMD) $(MAKE) $(subst docker_,,$@) \
	)

docker_component_test: docker_build
