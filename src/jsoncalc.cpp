#include <libgen.h>

#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stack>
#include <string>

#include "rapidjson/filereadstream.h"
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>

void usage(std::string prog) {
    std::cout << "Usage: " << prog << " <json_operand> <operator> <json_operand> [<operator> <json_operand>]..." << '\n';
    std::cout << "       " << prog << " -h" << '\n';
}

bool isOperator(std::string token) {
    return (token == "+") || (token == "-") || (token == "*") || (token == "/");
}

// Shunting-yard algorithm to convert infix to reverse polish notation.
std::string toRpn(std::string infix) {
    std::stack<std::string> operatorStack;
    std::ostringstream ostream;
    std::istringstream istream(infix);

    for(std::string token; istream >> token;) {
        if(isOperator(token)) {
            while(!operatorStack.empty() &&
                 ((( operatorStack.top() == "*" ||  operatorStack.top() == "/") && ( token == "+" || token == "-")) ||
                 (((operatorStack.top() == "*" || operatorStack.top() == "/") && (token == "*" || token == "/")) ||
                 ((operatorStack.top() == "+" || operatorStack.top() == "-") && (token == "+" || token == "-"))))) {
                ostream << operatorStack.top() << ' ';
                operatorStack.pop();
            }
            operatorStack.push(token);
        } else {
            ostream << token << ' ';
        }
    }

    while(!operatorStack.empty()) {
        ostream << operatorStack.top() << ' ';
        operatorStack.pop();
    }

    return ostream.str();
}

double toBase10(int radix, int value) {
    int base10Value = 0;
    auto str_value = std::to_string(value);
    int i = str_value.size() -1;

    for(auto ch: str_value) {
        base10Value += (ch - 48) * std::pow(radix, i);
        --i;
    }
    return base10Value;
}

double toDouble(std::string jsonFile) {

    std::ifstream ifs(jsonFile);
    if (!ifs.is_open()) {
        std::cerr << "Failed to open " << jsonFile << " for reading.\n";
        exit(1);
    }

    rapidjson::IStreamWrapper isw(ifs);
    rapidjson::Document doc;
    doc.ParseStream(isw);

    int radix = doc["radix"].GetInt();
    int value = doc["value"].GetInt();
    return toBase10(radix, value);
}

double calculate(std::string rpn) {
    std::stack<double> stack;
    std::istringstream istream(rpn);

    for(std::string token; istream >> token;) {

        if(isOperator(token)) {
            int ope2 = stack.top(); stack.pop();
            int ope1 = stack.top(); stack.pop();

            if(token == "+") {
                stack.push(ope1 + ope2);
            }
            else if(token == "-") {
                stack.push(ope1 - ope2);
            }
            else if(token == "*") {
                stack.push(ope1 * ope2);
            }
            else if(token == "/") {
                stack.push(ope1 / ope2);
            }
        } else {
            stack.push(std::stoi(token));
        }
    }

    return stack.top();
}

int main(int argc, char** argv) {
    std::string prog = basename(argv[0]);
    if(argc == 1 || !std::strcmp(argv[1], "-h")) {
        usage(prog);
        return 0;
    }
    if(argc % 2 == 1) {
        usage(prog);
        return 1;
    }

    std::ostringstream ostream;
    for(int i=1; i < argc; ++i) {
        if(i % 2 == 1) {
            ostream << toDouble(argv[i]) << ' ';
        } else {
            auto cur = argv[i];
            if(!isOperator(cur)) {
                usage(prog);
                return 1;
            }
            ostream << argv[i] << ' ';
        }
    }

    auto rpn = toRpn(ostream.str());
    auto res = calculate(rpn);
    std::cout << res << '\n';
    return 0;
}
